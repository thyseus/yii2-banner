<?php

use thyseus\banner\models\Banner;
use thyseus\files\models\File;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model thyseus\banner\models\Banner */
/* @var $form yii\widgets\ActiveForm */
$this->title = Yii::t('banner', 'Upload Banner: ') . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('banner', 'Banner'), 'url' => ['admin']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->slug]];
$this->params['breadcrumbs'][] = Yii::t('banner', 'Upload Banner: ');

?>

<div class="banner-form">
    <h1><?= Html::encode($this->title) ?></h1>


    <?php $form = ActiveForm::begin(); ?>


   <div>Use your file uploader</div>

    <?php ActiveForm::end(); ?>

</div>
