<?php

use yii\db\Migration;

/**
 * Handles adding position to table `banner`.
 */
class m181112_164446_add_position_column_to_banner_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('banner', 'position', $this->integer()->notNull());

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
    }
}
